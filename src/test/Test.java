/*
 * *************************************************************************
 * 
 * Copyright (c)  Eva TechSoft General License 1.0
 *
 * [2011] - [2013] Eva TechSoft Private Limited
 * All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Eva TechSoft Private Limited and its suppliers,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Eva TechSoft Private Limited
 * and its suppliers and may be covered by Indian and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Eva TechSoft Private Limited.
 *
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.evatechsoft.com
 *
 * *************************************************************************
 */
package test;

/**
 *
 * File : Test.java Encoding: UTF-8 Project: test Date : 3 Dec, 2013 1:42:30 PM
 *
 * Author : Chandan Pasunoori :: cp@evatechsoft.com
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        for (int i = 2; i <= 12; i++) {
            System.out.println("=Sum(j2:j" + i + ")");
        }
    }

}
